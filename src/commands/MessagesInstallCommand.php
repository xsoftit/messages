<?php

namespace Xsoft\Messages;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class MessagesInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Migrating' . PHP_EOL;
        Artisan::queue('migrate', ['-q' => '']);
        echo 'Seeding' . PHP_EOL;
        Artisan::queue('db:seed', ['-q' => '', '--class' => 'MessageConfigSeeder']);
        echo 'Publishing' . PHP_EOL;
//        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-controller", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-listener", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-starter", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-helper", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-email", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-seeder", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "xsoft-messages-exampleViews", '--force' => true]);
        $ready = $this->confirm('Insert " //x-soft_Messages " inside schedule function in app/Console/Kernel.php and confirm when you are done.' . PHP_EOL . 'This will enable sending emails and clearing old messages.', false);
        if ($ready) {
            echo 'Updating Kernel.php' . PHP_EOL;
            $kernel = file_get_contents(app_path('Console/Kernel.php'));
            $kernel = str_replace('//x-soft_Messages', '
        $schedule->command(\'messages:email:send\');
        $schedule->command(\'messages:clear\')->monthlyOn(1, \'01:00\');', $kernel);
            file_put_contents(app_path('Console/Kernel.php'), $kernel);
        } else {
            echo 'Kernel not updated. Emails and clearing old messages will not work automatically.' . PHP_EOL;
        }
        $ready = $this->confirm('Insert " //x-soft_Messages " inside app/Providers/EventServiceProvider.php class and confirm when you are done.' . PHP_EOL . 'This is necessary for MessageSender listener to work.', false);
        if ($ready) {
            $eventServiceProvider = file_get_contents(app_path('Providers/EventServiceProvider.php'));
            $eventServiceProvider = str_replace('//x-soft_Messages', 'protected $subscribe = [
        \'App\Listeners\MessageSender\',
    ];', $eventServiceProvider);
            file_put_contents(app_path('Providers/EventServiceProvider.php'), $eventServiceProvider);
            echo 'EventServiceProvider updated successfully' . PHP_EOL;
        } else {
            echo 'EventServiceProvider not updated. Events will not be handled by MessagesSender listener.' . PHP_EOL;
        }
        $ready = $this->confirm('Insert " //x-soft_Sent_Email_Handling " inside $listen array in app/Providers/EventServiceProvider.php and confirm when you are done.' . PHP_EOL . 'This is necessary for tag email messages as sent and prevent sending them again.', false);
        if ($ready) {
            $eventServiceProvider = file_get_contents(app_path('Providers/EventServiceProvider.php'));
            $eventServiceProvider = str_replace('//x-soft_Sent_Email_Handling', '\'Illuminate\Mail\Events\MessageSent\' => [
            \'App\Listeners\MessageEmailSentHandler\',
        ],', $eventServiceProvider);
            file_put_contents(app_path('Providers/EventServiceProvider.php'), $eventServiceProvider);
            echo 'EventServiceProvider updated successfully' . PHP_EOL;
        } else {
            echo 'EventServiceProvider not updated. Events will not be handled by MessagesSender listener.' . PHP_EOL;
        }
        if (!File::exists(app_path('Events'))) {
            File::makeDirectory(app_path('Events'));
        }
        echo 'Installation Complete' . PHP_EOL;
    }
}
