<?php

namespace Xsoft\Messages;

use App\Helpers\EmailMessageHelper;
use Illuminate\Console\Command;

class SendEmailMessagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $messageConfigs = MessageConfig::all();
        $eph = $messageConfigs->where('slug', 'eph')->first();
        if ($eph) {
            $emails_per_hour_counter = (int)$eph->extra;
            $emails_per_minute = $messageConfigs->where('slug', 'epm')->first()->value;
            if ($emails_per_hour_counter > 0 && $emails_per_minute > 0) {
                if ($emails_per_minute > $emails_per_hour_counter) {
                    $emails_per_minute = $emails_per_hour_counter;
                }
                $messages = Message::with('recipient')
                    ->where('channel', '=', 'email')
                    ->where('send_at', '=', null)
                    ->where('counter', '<', 10)
                    ->limit($emails_per_minute)
                    ->get();
                $messages_count = $messages->count();
                if ($messages_count > 0) {
                    $eph->update([
                        'extra' => (int)$eph->extra - $messages_count,
                    ]);
                    EmailMessageHelper::send($messages);
                }
            }
        }
    }
}
