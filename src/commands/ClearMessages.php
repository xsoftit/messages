<?php

namespace Xsoft\Messages;

use Illuminate\Console\Command;

class ClearMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lp = MessageConfig::where('slug', 'ls')->first();
        $messages = Message::where('created_at', '>=', now()->subMonths($lp->value)->toDateTimeString())->get();
        $count = $messages->count();
        $messages->delete();
        echo $count . ' notifications deleted' . PHP_EOL;
    }
}
