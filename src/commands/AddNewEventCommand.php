<?php

namespace Xsoft\Messages;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class AddNewEventCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:new:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $eventName = $this->ask('Enter event name: ');
        $eventName = str_replace(' ', '', $eventName);
        $eventName = str_replace('.', '', $eventName);
        $eventName = str_replace('/', '', $eventName);
        $eventName = str_replace(',', '', $eventName);
        echo 'New event name: ' . $eventName . PHP_EOL;
        $messageTemplateContentBladeName = $this->ask('Enter Message Template Content Blade Name: ');
        if (!File::exists(app_path('Events'))) {
            File::makeDirectory(app_path('Events'));
        }
        $eventFile = file_get_contents(__DIR__ . '/Starter/CustomEvent.php');
        $eventFile = str_replace('CustomEvent', $eventName, $eventFile);
        file_put_contents(app_path('Events/' . $eventName . '.php'), $eventFile);
        $listener = file_get_contents(app_path('Listeners/MessageSender.php'));
        $listener = str_replace('//listen_to_new_event', '$events->listen(
            \'App\Events\\' . $eventName . '\',
            \'App\Listeners\MessageSender@on' . $eventName . '\'
        );
        //listen_to_new_event', $listener);
        $listener = str_replace('//handle_new_event', 'public function on' . Str::ucfirst($eventName) . '($event)
    {
        $recipients = $event->recipients; //collection of User model objects
        $contextData = [
            \'Recipients\' => $recipients,
        ];
        $this->send(\'' . $messageTemplateContentBladeName . '\', $contextData);
    }
    //handle_new_event', $listener);
        file_put_contents(app_path('Listeners/MessageSender.php'), $listener);
        echo 'Update new event as you need and call your event by: event(new ' . $eventName . '($recipients));' . PHP_EOL;
        echo 'Remember to update MessageSender Listener' . PHP_EOL;
    }
}
