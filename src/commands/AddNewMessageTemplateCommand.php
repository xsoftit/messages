<?php

namespace Xsoft\Messages;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class AddNewMessageTemplateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:new:template';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $template_name = $this->ask('Enter new template name. ' . PHP_EOL . 'This will also be the title of email message.');
        $system = $this->confirm('Is this template system message? :', true);
        $email = $this->confirm('Is this template email message? :', true);
        $channels = [];
        if ($system) {
            $channels[] = 'system';
        }
        if ($email) {
            $channels[] = 'email';
        }
        if (!$system && !$email) {
            echo 'Cannot create template without assigned chanel' . PHP_EOL;
            return 0;
        }
        $messageTemplate = "
        if(!MessageTemplate::where('name','" . $template_name . "')->first()){
            MessageTemplate::create([
                'name' => '" . $template_name . "',
                'content_blade_name' => '" . Str::camel($template_name) . "',
                'channels' => '" . json_encode($channels) . "'
            ]);
            echo 'Seeded template: " . $template_name . "' . PHP_EOL;
        }
        //NEW_TEMPLATE_HERE";
        $seeder = file_get_contents(database_path('seeds/') . 'MessageTemplatesSeeder.php');
        $seeder = str_replace('//NEW_TEMPLATE_HERE', $messageTemplate, $seeder);
        file_put_contents(database_path('seeds/') . 'MessageTemplatesSeeder.php', $seeder);

        if (File::exists(resource_path('views/messageTemplates/starter.blade.php'))) {
            copy(resource_path('views/messageTemplates/starter.blade.php'), resource_path('views/messageTemplates/' . Str::camel($template_name) . '.blade.php'));
        }
        Artisan::queue('db:seed --class=MessageTemplatesSeeder');
        echo 'Created views/messageTemplates/.' . Str::camel($template_name) . '.blade.php' . PHP_EOL . PHP_EOL;

    }
}
