<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Xsoft\Messages\Message;

class MessageEmailSentHandler
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        if (array_key_exists('email', $event->data)) {
            $email = $event->data['email']->receiverEmail;
            $message = Message::where('recipient_email', $email)->where('send_at', null)->where('title', $event->data['email']->title)->where('content', $event->data['email']->content)->first();
            if ($message) {
                $message->update([
                    'send_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }
}
