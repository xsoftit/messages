<?php

namespace App\Listeners;

use Xsoft\Messages\Message;

class MessageSender
{
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\CustomEvent',
            'App\Listeners\MessageSender@onCustomEvent'
        );
//        $events->listen(
//            'App\Events\CustomEvent2',
//            'App\Listeners\MessageSender@onCustomEvent2'
//        );
        //listen_to_new_event
    }

    public function onCustomEvent($event)
    {
        $recipients = $event->recipients; //collection of User model objects
        $contextData = [
            'Recipients' => $recipients,
        ];
        $this->send('bladeName', $contextData);
    }

    //handle_new_event

    private function send($content_blade_name, $contextData)
    {
        Message::send($content_blade_name, $contextData);
    }
}
