<?php

namespace App\Http\Controllers\Messages;

use Xsoft\Messages\MainMessageController;
use Illuminate\Http\Request;

class MessageController extends MainMessageController
{
    private $max_values = [
        'epm' => 30,
        'eph' => 1000,
        'ls' => 365
    ];
    private $min_values = [
        'epm' => 0,
        'eph' => 0,
        'ls' => 30
    ];
}
