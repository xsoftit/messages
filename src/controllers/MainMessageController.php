<?php

namespace Xsoft\Messages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainMessageController extends Controller
{
    private $max_values = [
        'epm' => 30,
        'eph' => 1000,
        'ls' => 12
    ];
    private $min_values = [
        'epm' => 0,
        'eph' => 0,
        'ls' => 1
    ];

    public function messagesConfig()
    {
        $messageConfigs = MessageConfig::all();
        $templates = MessageTemplate::all();
        foreach ($templates as &$template) {
            $channels = json_decode($template->channels, true);
            foreach ($channels as $channel) {
                $template->$channel = true;
            }
        }
        return view('messages.messagesConfig', [
            'messageConfigs' => $messageConfigs,
            'max_values' => $this->max_values,
            'min_values' => $this->min_values,
            'templates' => $templates
        ]);
    }

    public function configUpdate(Request $request)
    {
        $configs = MessageConfig::all();
        foreach ($request->get('config') as $slug => $value) {
            $configs->where('slug', $slug)->first()->update(['value' => $value]);
        }
        return redirect()->route('messages.config');
    }

    public function toggleChannel(Request $request, MessageTemplate $messageTemplate)
    {
        $channel_type = $request->type;
        $channels = json_decode($messageTemplate->channels);
        if (in_array($channel_type, $channels)) {
            $new_channels = [];
            foreach ($channels as $channel) {
                if ($channel != $channel_type) {
                    $new_channels[] = $channel;
                }
            }
            $channels = $new_channels;
        } else {
            $channels[] = $channel_type;
        }
        $messageTemplate->update([
            'channels' => json_encode($channels),
        ]);

        return json_encode(['selected_type' => $channel_type, 'channels' => $channels]);
    }
}
