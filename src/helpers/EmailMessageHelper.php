<?php

namespace App\Helpers;

use App\Mail\EmailMessage;
use Illuminate\Support\Facades\Mail;

class EmailMessageHelper
{

    static public function send($messages)
    {
        foreach ($messages as $message) {
            if ($message->recipient) {
                $data = [
                    'title' => $message->title,
                    'content' => $message->content,
                    'receiverEmail' => $message->recipient->email
                ];
                $email = new \stdClass();
                foreach ($data as $key => $value) {
                    $email->$key = $value;
                }
                $message->update([
                    'recipient_email' => $message->recipient->email,
                    'counter' => $message->counter++
                ]);
                $response = Mail::to($data['receiverEmail'])->send(new EmailMessage($email));
                if (count(Mail::failures()) > 0) {
                    throw new \ErrorException('Failed to send Mail ' . $message->id . ' | ' . $response);
                }
            }
        }
    }
}
