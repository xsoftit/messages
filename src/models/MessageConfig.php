<?php

namespace Xsoft\Messages;

use Illuminate\Database\Eloquent\Model;

class MessageConfig extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'description',
        'value',
        'extra',
    ];
}
