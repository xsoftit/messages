<?php

namespace Xsoft\Messages;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'title',
        'content',
        'recipient_id',
        'recipient_email',
        'channel',
        'counter',
        'send_at',
        'read_at'
    ];

    public function recipient()
    {
        return $this->belongsTo('App\User', 'recipient_id');
    }

    static public function send($blade_name, $contextData)
    {
        $template = MessageTemplate::where('content_blade_name', $blade_name)->first();
        $channel = json_decode($template->channels);
        $data = [];
        foreach ($contextData as $category => $objects) {
            if ($category != 'Recipients') {
                $data[$category] = $objects;
            }
        }
        $recipients = $contextData['Recipients'];
        foreach ($recipients as $recipient) {
            $view = view('messageTemplates.' . $template->content_blade_name, ['recipient' => $recipient, 'data' => $data]);
            $content = $view->render();
            $message = new Message([
                'title' => $template->name,
                'content' => $content,
                'recipient_id' => $recipient->id,
                'recipient_email' => $recipient->email,
            ]);
            if (in_array('system', $channel)) {
                $message->channel = 'system';
                $message->send_at = date('Y-m-d H:i:s');
                $message->save();
            }
            if (in_array('email', $channel)) {
                $email_message = $message->replicate();
                $email_message->send_at = null;
                $email_message->channel = 'email';
                $email_message->save();
            }
        }
    }
}
