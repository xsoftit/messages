<?php

namespace Xsoft\Messages;

use Illuminate\Database\Eloquent\Model;

class MessageTemplate extends Model
{
    protected $fillable = [
        'name',
        'content_blade_name',
        'channels'
    ];
}
