<?php

namespace Xsoft\Messages;

use Illuminate\Support\ServiceProvider;

class MessagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->publishes([
            __DIR__ . '/controllers/EmptyMessageController.php' => app_path('/Http/Controllers/Messages/MessageController.php'),
        ], 'xsoft-messages-controller');
        $this->publishes([
            __DIR__ . '/listeners/MessageSender.php' => app_path('/Listeners/MessageSender.php'),
            __DIR__ . '/listeners/MessageEmailSentHandler.php' => app_path('/Listeners/MessageEmailSentHandler.php'),
        ], 'xsoft-messages-listener');
        $this->publishes([
            __DIR__ . '/commands/Starter/starter.blade.php' => resource_path('/views/messageTemplates/starter.blade.php'),
            __DIR__ . '/../resources/views/messages' => resource_path('/views/messages'),
        ], 'xsoft-messages-starter');
        $this->publishes([
            __DIR__ . '/helpers/EmailMessageHelper.php' => app_path('Helpers/EmailMessageHelper.php')
        ], 'xsoft-messages-helper');
        $this->publishes([
            __DIR__ . '/mail/EmailMessage.php' => app_path('Mail/EmailMessage.php'),
            __DIR__ . '/../resources/views/emails' => resource_path('views/emails')
        ], 'xsoft-messages-email');
        $this->publishes([
            __DIR__ . '/seeder/MessageTemplatesSeeder.php' => database_path('seeds/MessageTemplatesSeeder.php')
        ], 'xsoft-messages-seeder');
        $this->commands([
            MessagesInstallCommand::class,
            AddNewMessageTemplateCommand::class,
            AddNewEventCommand::class,
            SendEmailMessagesCommand::class,
            ClearMessages::class,
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/../routes/web.php';
        $this->app->make('Xsoft\Messages\MainMessageController');
    }
}
