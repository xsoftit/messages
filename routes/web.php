<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('messages')->name('messages.')->group(function () {
    Route::get('/config', 'xsoft\messages\MainMessageController@messagesConfig')->name('config')->middleware('permission:messages.config');
    Route::post('/config', 'xsoft\messages\MainMessageController@configUpdate')->name('configUpdate')->middleware('permission:messages.config');
    Route::post('/toggleChannel', 'xsoft\messages\MainMessageController@toggleChannel')->name('toggleChannel')->middleware('permission:messages.config');
});
