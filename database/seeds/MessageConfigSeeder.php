<?php

use Illuminate\Database\Seeder;
use Xsoft\Messages\MessageConfig;

class MessageConfigSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!MessageConfig::where('slug', 'eph')->first()) {
            MessageConfig::create([
                'name' => 'Emails per hour',
                'slug' => 'eph',
                'description' => 'Maximum number of emails that can be send within an hour.',
                'value' => '500',
                'extra' => '500'
            ]);
            echo 'Seeded Emails per hour' . PHP_EOL;
        }
        if (!MessageConfig::where('slug', 'epm')->first()) {
            MessageConfig::create([
                'name' => 'Emails per minute',
                'slug' => 'epm',
                'description' => 'Maximum number of emails that can be send within a minute.',
                'value' => '10',
                'extra' => '10'
            ]);
            echo 'Seeded Emails per minute' . PHP_EOL;
        }
        if (!MessageConfig::where('slug', 'ls')->first()) {
            MessageConfig::create([
                'name' => 'Life span',
                'slug' => 'ls',
                'description' => 'Maximum lifespan of single Message. It will be deleted after given number of months.',
                'value' => '3',
            ]);
            echo 'Seeded Emails per minute' . PHP_EOL;
        }
    }
}
