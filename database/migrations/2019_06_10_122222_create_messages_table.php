<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('content');
            $table->integer('recipient_id');
            $table->string('recipient_email')->nullable();
            $table->string('channel')->default('system');//system, email
            $table->integer('counter')->default(0);
            $table->timestamps();
            $table->timestamp('send_at')->nullable()->default(null);
            $table->timestamp('read_at')->nullable()->default(null);
            $table->index('recipient_id');
            $table->index('channel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
