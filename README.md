# Installation

Run commands:

    composer require xsoft/messages
    php artisan messages:install

# Usage
##Overridable routes
```$xslt
Route::prefix('messages')->name('messages.')->group(function () {
    Route::get('/config', 'xsoft\messages\MainMessageController@messagesConfig')->name('config')->middleware('permission:messages.config');
    Route::post('/config', 'xsoft\messages\MainMessageController@configUpdate')->name('configUpdate')->middleware('permission:messages.config');
    Route::post('/toggleChannel', 'xsoft\messages\MainMessageController@toggleChannel')->name('toggleChannel')->middleware('permission:messages.config');
});
```
## Adding new message template
Run command and follow instructions

    php artisan maessages:new:template

This command will create template blade inside resources/views/messageTemplates. You can customize your message in that file.

## Adding new event
Run command and follow instructions:

    php artisan messages:new:event
    
This command will create new event inside app/Events directory and update MessageSender listener however you should update it according to your needs.

## Sending message
Simply insert into your code: 

    event(new eventName(recipients, data1, data2, ...));
    
### Data travel
eventTrigger -\> event -\> MessageSender (listener) -\> Message (model) -\> MessageTemplate (blade)
### Handling data inside blade template
Recipient data is stored in `$recipient` variable and any additional data in `$data` array where index is defined in MessageSender (listener)
