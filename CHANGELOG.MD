#1.1.7
* added `read_at` to `fillable` variable in `Message` model

#1.1.6
* system messages now also have title saved

#1.1.4
* moved `MessageTemplateSeeder` to different directory in order to eliminate ambiguous class resolution warning on `composer dump-autoload` command

#1.1.2
* Fixed `messages:new:template` command
* Optimized `messages:clear` command
* Fixed `toggleChannel` function in MainMessageController
* Merged `templatesConfig.blade.php` to `messagesConfig.blade.php`
* Adjusted `MessageConfigSeeder` to seed proper init value for message lifespan config

#1.1

##Additions
* publishable `MessageTemplatesSeeder` under `xsoft-messages-seeder`
* publishable `messagesConfig` and `templatesConfig` blades under `xsoft-messages-exampleViews`
* functions to `MainMessageController`
* `routes/web.php`

##Changes
* `messages:new:template` command now adds entry to `MessageTemplatesSeeder` instead of creating entry in database
* slug for `Life span` config changed from `lp` to `ls` 
* `messages:install` command publishes `MessageTemplatesSeeder`
* `messages:email:send` reduced number of queries
