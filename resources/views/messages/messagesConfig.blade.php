<section>
    <header>
        <div>
            <span></span>
        </div>
    </header>
    <div>
        <table>
            <thead>
            <th>id</th>
            <th>name</th>
            <th>description</th>
            <th>slug</th>
            <th>value</th>
            <th>extra</th>
            <th></th>
            </thead>
            <tbody>
            <form action="{{route('messages.configUpdate')}}" method="POST">
                @foreach($messageConfigs as $config)
                    <tr>
                        @csrf
                        @method("POST")
                        <td>{{$config->id}}</td>
                        <td>{{$config->name}}</td>
                        <td>{{$config->description}}</td>
                        <td>{{$config->slug}}</td>
                        <td><input name="config[{{$config->slug}}]" value="{{$config->value}}" type="number"
                                   min="{{$min_values[$config->slug]}}" max="{{$max_values[$config->slug]}}">
                        </td>
                        <td>{{$config->extra}}</td>
                        </td>
                        <td>
                            <button type="submit">Save</button>
                        </td>
                    </tr>
                @endforeach
            </form>
            </tbody>
        </table>
    </div>
</section>
<section>
    <header>
        <div>
            <span>Messages channels config</span>
        </div>
    </header>
    <div>
        <table>
            <col width="80%">
            <col width="10%">
            <col width="10%">
            <thead>
            <tr>
                <th>Template</th>
                <th>System</th>
                <th>E-mail</th>
            </tr>
            </thead>
            <tbody>
            @foreach($templates as $template)
                <tr>
                    <td>{{$template->name}}</td>
                    <td>
                        <div>
                            <input class="template_checkbox" type="checkbox" data-type="system"
                                   data-id="{{$template->id}}" {{$template->system ? "checked": ""}}>
                        </div>
                    </td>
                    <td>
                        <div>
                            <input class="template_checkbox" type="checkbox" data-type="email"
                                   data-id="{{$template->id}}" {{$template->email ? "checked": ""}}>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>
<script>
    $('.template_checkbox').on("click", function () {
        let url = "{{route('messages.toggleChannel',['messageTemplate'=>'NT_PLACEHOLDER'])}}";
        url = url.replace('NT_PLACEHOLDER', $(this).data('id'));
        $.ajax({
            type: 'POST',
            url: url,
            data: {type: $(this).data('type')},
            success: function (response) {

            }
        });
    })
</script>
